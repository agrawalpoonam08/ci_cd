Files are:

File: **.gitlab-ci.yml** to deploy ci/cd pipeline, it builds the image with the Dockerfile.

File: **docker-compose.yaml** to deploy on staging and testing environment.

File: **kubernetes-manifest.yaml**, to deploy two pods each on single node.
