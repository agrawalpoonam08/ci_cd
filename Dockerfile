From ubuntu:latest
RUN apt-get update -y && apt-get install maven openjdk-8-jdk git -y
WORKDIR /app
RUN git clone https://gitlab.com/agrawalpoonam08/ip-config.git
WORKDIR /app/ip-config/
RUN mvn install -X
RUN mvn clean package 
WORKDIR /app/ip-config/target/
CMD ["java","-jar", "ip-config-0.0.1-SNAPSHOT.jar"]
